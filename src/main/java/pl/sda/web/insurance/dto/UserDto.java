/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.sda.web.insurance.dto;

/**
 *
 * @author RWSwiss
 */
public class UserDto {
    
    private Long userId;
    private String firstName;
    private String lastName;
    private int pesel;

    public Long getUserId() {
        return userId;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public int getPesel() {
        return pesel;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setPesel(int pesel) {
        this.pesel = pesel;
    }
    
    
}
