/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.sda.web.insurance.dto;

import java.util.Date;

/**
 *
 * @author RWSwiss
 */
public class EngineDto {
    
    private Long engineId;
    private Long engineNumber;
    private Date engineProductionDate;

    public Long getEngineId() {
        return engineId;
    }

    public Long getEngineNumber() {
        return engineNumber;
    }

    public Date getEngineProductionDate() {
        return engineProductionDate;
    }

    public void setEngineId(Long engineId) {
        this.engineId = engineId;
    }

    public void setEngineNumber(Long engineNumber) {
        this.engineNumber = engineNumber;
    }

    public void setEngineProductionDate(Date engineProductionDate) {
        this.engineProductionDate = engineProductionDate;
    }
    
    
}
