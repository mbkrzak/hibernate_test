/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.sda.web.insurance.dto;

import java.util.Date;

/**
 *
 * @author RENT
 */
public class FormDto {

    private AddressDto addressDto;
    private CarDataDto carDataDto;
    private CarDto carDto;
    private CarTypeDto carTypeDto;
    private EngineDto engineDto;
    private UserDto userDto;

    public AddressDto getAddressDto() {
        return addressDto;
    }
    
    public CarDataDto getCarDataDto() {
        return carDataDto;
    }

    public CarDto getCarDto() {
        return carDto;
    }

    public CarTypeDto getCarTypeDto() {
        return carTypeDto;
    }

    public EngineDto getEngineDto() {
        return engineDto;
    }

    public UserDto getUserDto() {
        return userDto;
    }

    public void setAddressDto(AddressDto addressDto) {
        this.addressDto = addressDto;
    }    
    
    public void setCarDataDto(CarDataDto carDataDto) {
        this.carDataDto = carDataDto;
    }

    public void setCarDto(CarDto carDto) {
        this.carDto = carDto;
    }

    public void setCarTypeDto(CarTypeDto carTypeDto) {
        this.carTypeDto = carTypeDto;
    }

    public void setEngineDto(EngineDto engineDto) {
        this.engineDto = engineDto;
    }

    public void setUserDto(UserDto userDto) {
        this.userDto = userDto;
    }
    
    
   
    
}
