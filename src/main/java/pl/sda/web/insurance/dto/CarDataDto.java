/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.sda.web.insurance.dto;

import java.util.Date;

/**
 *
 * @author RWSwiss
 */
public class CarDataDto {

    private Long carDataId;
    private String registrationNr;
    private Long vinNr;
    private Date firstRegistration;
    private Date carProductionDate;

    public Long getCarDataId() {
        return carDataId;
    }

    public String getRegistrationNr() {
        return registrationNr;
    }

    public Long getVinNr() {
        return vinNr;
    }

    public Date getFirstRegistration() {
        return firstRegistration;
    }

    public Date getCarProductionDate() {
        return carProductionDate;
    }

    public void setCarDataId(Long carDataId) {
        this.carDataId = carDataId;
    }

    public void setRegistrationNr(String registrationNr) {
        this.registrationNr = registrationNr;
    }

    public void setVinNr(Long vinNr) {
        this.vinNr = vinNr;
    }

    public void setFirstRegistration(Date firstRegistration) {
        this.firstRegistration = firstRegistration;
    }

    public void setCarProductionDate(Date carProductionDate) {
        this.carProductionDate = carProductionDate;
    }
    
    
    
}
