/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.sda.web.insurance;

import org.hibernate.*;
import java.util.*;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;

import org.modelmapper.ModelMapper;
import pl.sda.web.insurance.dto.*;
import pl.sda.web.insurance.model.*;


/**
 *
 * @author RWSwiss
 */
@ManagedBean(name = "applicationManagedBean")
@ApplicationScoped
public class ApplicationManagedBean {


    public List<UserDto> getUsersDto()
    {
        System.out.println("Hello World");
        SessionFactory instance = ConfigHibernate.getInstance();            
        Session session = instance.openSession();            
        Transaction beginTransaction = session.beginTransaction();
        
        Query query = session.createQuery("From User");
        List<User> users = query.list();
        List<UserDto> usersDto = new ArrayList<UserDto>();
        
        if(users.size()>0)
        {     
            //Lambda doesn't work!!
            //List<UserDto> usersDto = users.stream().map(u -> (UserDto)getMappedObject(u, new UserDto())).collect(Collectors.toList());
            
            for(User u : users)
            {
                UserDto userDto = (UserDto)getMappedObject(u, new UserDto());
                usersDto.add(userDto);
            }
        }
        
        return usersDto;
    }
    
    
    private Object getMappedObject(Object start, Object destination) {
        ModelMapper mapper = new ModelMapper();
        Object map = mapper.map(start, destination.getClass());
        return map;
    }
    
}
