/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.sda.web.insurance.model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.*;


/**
 *
 * @author RENT
 */
@Entity
@Table(name="users")
public class User implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    
    @Column(name= "first_name")
    private String firstName;
    
    @Column(name="last_name")
    private String lastName;
    
    @Column
    private int pesel;
    
    @ManyToOne
    @JoinColumn(name="address_id")
    private Address address;
    
    @ManyToMany(mappedBy="users")
    private List<Car> cars;

    public User() {
    }

    public User(User user, int i) {
        this.firstName = user.getFirstName() + i;
        this.lastName = user.getLastName();
        this.pesel = user.getPesel();
        this.address = user.getAddress();
    }    
    
    
    public List<Car> getCar() {
        return cars;
    }

    public void setCar(List<Car> cars) {
        this.cars = cars;
    }
    

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    
    
    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public int getPesel() {
        return pesel;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setPesel(int pesel) {
        this.pesel = pesel;
    }
    
    
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof User)) {
            return false;
        }
        User other = (User) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "pl.sda.web.insurance.model.NewEntity[ id=" + id + " ]";
    }
    
}
