/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.sda.web.insurance.model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.*;


/**
 *
 * @author RENT
 */
@Entity
@Table(name="cars")
public class Car implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @OneToOne
    @JoinColumn(name="car_data_id")
    private CarData carData;
    
    @ManyToOne
    @JoinColumn(name="car_type_id")
    private CarType cartype;
    
    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable( name = "Car_User",
            joinColumns = @JoinColumn(name = "car_id", referencedColumnName="id"),
            inverseJoinColumns = @JoinColumn(name = "user_id", referencedColumnName="id"))
    private List<User> users;

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

    
    public CarData getCarData() {
        return carData;
    }

    public CarType getCartype() {
        return cartype;
    }

    public void setCarData(CarData carData) {
        this.carData = carData;
    }

    public void setCartype(CarType cartype) {
        this.cartype = cartype;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Car)) {
            return false;
        }
        Car other = (Car) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "pl.sda.web.insurance.model.Car[ id=" + id + " ]";
    }
    
}
