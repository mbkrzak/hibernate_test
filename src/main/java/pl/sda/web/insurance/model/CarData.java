/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.sda.web.insurance.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;

/**
 *
 * @author RENT
 */
@Entity
public class CarData implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
            
    @Column(name="registration_number")
    private String registrationNr;
    
    @Column(name="vin_number")
    private Long vinNr;
    
    @Column(name="first_registration")
    private Date firstRegistration;
    
    @Column(name="production_date")
    private Date productionDate;   

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getRegistrationNr() {
        return registrationNr;
    }

    public Long getVinNr() {
        return vinNr;
    }

    public Date getFirstRegistration() {
        return firstRegistration;
    }

    public Date getProductionDate() {
        return productionDate;
    }

    public void setRegistrationNr(String registrationNr) {
        this.registrationNr = registrationNr;
    }

    public void setVinNr(Long vinNr) {
        this.vinNr = vinNr;
    }

    public void setFirstRegistration(Date firstRegistration) {
        this.firstRegistration = firstRegistration;
    }

    public void setProductionDate(Date productionDate) {
        this.productionDate = productionDate;
    }
    
    

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CarData)) {
            return false;
        }
        CarData other = (CarData) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "pl.sda.web.insurance.model.CarData[ id=" + id + " ]";
    }
    
}
