/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.sda.web.insurance.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;


/**
 *
 * @author RENT
 */
@Entity
@Table(name="engines")
public class Engine implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    
    @Column(name="engine_number")
    private Long engineNumber;
    
    @Column(name="production_name")
    private Date productionDate;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setEngineNumber(Long engineNumber) {
        this.engineNumber = engineNumber;
    }

    public void setProductionDate(Date productionDate) {
        this.productionDate = productionDate;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Long getEngineNumber() {
        return engineNumber;
    }

    public Date getProductionDate() {
        return productionDate;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Engine)) {
            return false;
        }
        Engine other = (Engine) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "pl.sda.web.insurance.model.Engine[ id=" + id + " ]";
    }
    
}
