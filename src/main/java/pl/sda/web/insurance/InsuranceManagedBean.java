/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.sda.web.insurance;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import org.hibernate.Session;

import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.modelmapper.ModelMapper;
import pl.sda.web.insurance.dto.*;
import pl.sda.web.insurance.model.*;

/**
 *
 * @author RENT
 */
@ManagedBean(name = "insuranceManagedBean")
@RequestScoped
public class InsuranceManagedBean {

    
    FormDto formDto;
    
    @PostConstruct
    public void init() {
        formDto= new FormDto();
        formDto.setUserDto(new UserDto());
        formDto.setAddressDto(new AddressDto());
        formDto.setCarDto(new CarDto());
        formDto.setCarDataDto(new CarDataDto());
        formDto.setCarTypeDto(new CarTypeDto());
        formDto.setEngineDto(new EngineDto());
    }

    public FormDto getFormDto() {
        return formDto;
    }

    public void setFormDto(FormDto formDto) {
        this.formDto = formDto;
    }
    
    public void save()
    {
        ModelMapper mapper = new ModelMapper();
        Address add = new Address();
        mapper.map(formDto.getAddressDto(), add);
        
        Address address = (Address)getMappedObject(formDto.getAddressDto(), new Address());
        User user = (User)getMappedObject(formDto.getUserDto(), new User());
        CarData carData = (CarData)getMappedObject(formDto.getCarDataDto(), new CarData());
        CarType carType = (CarType)getMappedObject(formDto.getCarTypeDto(), new CarType());
        Engine engine = (Engine)getMappedObject(formDto.getEngineDto(), new Engine());
                            
        SessionFactory instance = ConfigHibernate.getInstance();            
        Session session = instance.openSession();            
        Transaction beginTransaction = session.beginTransaction(); 
        
        try {                        
                                
            Car car = new Car();
            
            //Define car type
            session.save(engine);
            carType.setEngine(engine); 
            session.save(carType);
            car.setCarData(carData);
            
            //define model
            session.save(carData); 
            car.setCartype(carType);
            
            //Define users
            List<User> users = new ArrayList<User>();
            session.save(address);          
            user.setAddress(address);
            //users.add(user);
            for(int i =0; i<10; i++)
            {
                User userNew  = new User(user,i);
                //userNew.setFirstName("Jacek_"+ i);
                //userNew = user;
                users.add(userNew);
            }
            //------
            
            car.setUsers(users);
                        
            session.save(car);
            
        } catch (Exception e) {
            System.out.println(e);
            beginTransaction.rollback();
        }
        beginTransaction.commit();        
    
    }
    

    /*public List<RentDto> getList(){
      List<Rent> rentBooks = ubi.getRentBooks(1L);
      List<RentDto> dtos = rentBooks.stream().map(this::map).collect(Collectors.toList());
      return dtos;
    }
    */

    private Object getMappedObject(Object start, Object destination) {
        ModelMapper mapper = new ModelMapper();
        Object map = mapper.map(start, destination.getClass());
        return map;
    }
    
    
}
